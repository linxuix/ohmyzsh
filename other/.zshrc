export ZSH=$HOME/.oh-my-zsh
ZSH_THEME="robbyrussell"
plugins=(git git-flow autojump redis-cli)

source $ZSH/oh-my-zsh.sh

export PATH=$HOME/bin:/usr/local/bin:$PATH

# my aliases
source ~/.aliases

# 提示符设置
PROMPT='%{$fg[yellow]%}%n@%m %{$fg[cyan]%}%c %{$fg[blue]%}$(git_prompt_info)%{$fg[red]%}$%{$reset_color%} '

